package br.com.cotemig.pAPP_Filas.controller;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.cotemig.pAPP_Filas.model.Persona;
import br.com.cotemig.pAPP_Filas.model.Produto;
import br.com.cotemig.pAPP_Filas.restController.BaseRestController;
import br.com.cotemig.pAPP_Filas.service.PersonaProdutoService;
import br.com.cotemig.pAPP_Filas.service.PersonaService;
import br.com.cotemig.pAPP_Filas.service.ProdutoService;

@Controller
public class PersonaProdutoController extends BaseRestController{

	
	@Autowired
	private PersonaProdutoService personaProdutoService;
	
	@Autowired
	private ProdutoService produtoService;
	
	@Autowired
	private PersonaService personaService;
	
	
	
	@RequestMapping(value = "/personaproduto/insert", method = RequestMethod.GET)
	public ModelAndView insert(Long idPersona) {
		
		ModelAndView mav = new ModelAndView("insertPersonaProduto");
		mav.addObject("persona", personaService.getPersonaById(idPersona).get());
	    mav.addObject("listaProdutos", produtoService.getAllProduto());
		
		return mav;
	}
	 
	@RequestMapping(value = "/personaproduto/insert", method = RequestMethod.POST)
	public String submitInsert(@Valid @ModelAttribute("persona") Persona persona, @ModelAttribute("listaProdutos") ArrayList<Produto> listaProduto,
							   BindingResult result, ModelMap model) {
	        
		if (result.hasErrors()) {
			return "error";
		}
		        
		personaProdutoService.insertListaProdutoNoPersona(getPersonaEx(), (ArrayList<Produto>) getListaProduto());
		
		
		return "redirect:/persona/index";
	}
	
	@RequestMapping(value = "/personaproduto/insertlistaproduto", method = RequestMethod.GET)
	public String insertListaProduto(Integer id, boolean add) {
	    
		Produto produtoEx = new Produto();
		
		for(Produto prod : getListaProdutoEx()) {
			if(prod.getId().equals(id)) {
				produtoEx = prod;
				break;
			}
		}
		

		getListaProduto().add(produtoEx);

			
		
		return "redirect:/personaproduto/realizarcompra?id="+getPersonaEx().getId();
	}
	
	
	@RequestMapping(value = "/personaproduto/buscarcompras", method = RequestMethod.GET)
	public ModelAndView buscarCompras(Long id) {
		
		Persona persona = personaService.getPersonaById(id).get();
		
		ModelAndView mav = new ModelAndView("updatePersonaProduto");
		mav.addObject("persona", persona);
	    mav.addObject("listaPersonaProdutos", personaProdutoService.buscarTodosProdutosDoPersona(persona));
		
		return mav;
	}
	
	@RequestMapping(value = "/personaproduto/realizarcompra", method = RequestMethod.GET)
	public ModelAndView realizarCompra(Long id) {
		
		setPersonaEx(personaService.getPersonaById(id).get());
		
		if(getListaProdutoEx().isEmpty())
			getListaProdutoEx().addAll(produtoService.getAllProduto());
		
		
		ModelAndView mav = new ModelAndView("comprarPersonaProduto");
		mav.addObject("persona", getPersonaEx());
	    mav.addObject("listaProdutos", getListaProdutoEx());
		
		return mav;
	}
	
	
}
