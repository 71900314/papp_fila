package br.com.cotemig.pAPP_Filas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PAppFilasApplication {

	public static void main(String[] args) {
		SpringApplication.run(PAppFilasApplication.class, args);
	}

}
