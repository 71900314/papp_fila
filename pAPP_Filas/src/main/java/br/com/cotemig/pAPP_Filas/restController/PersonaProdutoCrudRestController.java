package br.com.cotemig.pAPP_Filas.restController;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.cotemig.pAPP_Filas.model.Persona;
import br.com.cotemig.pAPP_Filas.model.PersonaProduto;
import br.com.cotemig.pAPP_Filas.model.Produto;
import br.com.cotemig.pAPP_Filas.service.PersonaProdutoService;

@RestController
public class PersonaProdutoCrudRestController extends BaseRestController {
	
	@Autowired
	private PersonaProdutoService personaProdutoService;

	
	@RequestMapping(value = "/teste/personaproduto/post", method = RequestMethod.POST)
	public void post(Persona persona, Produto produto, HttpServletResponse httpResponse) throws IOException {
	    PersonaProduto personaProduto = new PersonaProduto();
	    personaProduto.setPersona(persona);
	    personaProduto.setProduto(produto);
		httpResponse.sendRedirect("/form");
	}
	
	@RequestMapping(value = "/rest/personaproduto/getAll", method = RequestMethod.GET)
	public List<PersonaProduto> getAllPersonaProdutos() {
		return personaProdutoService.getAllPersonaProduto();	
	}
	
	@RequestMapping(value = "/rest/personaproduto/get/{id}", method = RequestMethod.GET)
	public Optional<PersonaProduto> getProdutoById(@PathVariable("id") Long id) {
		return personaProdutoService.getPersonaProdutoById(id);
	}
	
	@RequestMapping(value = "/rest/personaproduto/deleteAll", method = RequestMethod.DELETE)
	public void deleteAllProdutos() {
		personaProdutoService.deleteAllPersonaProduto();
	}
}
