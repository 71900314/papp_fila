package br.com.cotemig.pAPP_Filas.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.cotemig.pAPP_Filas.model.Persona;
import br.com.cotemig.pAPP_Filas.model.PersonaProduto;
import br.com.cotemig.pAPP_Filas.model.Produto;


public interface PersonaProdutoService {
	
	Optional<PersonaProduto> getPersonaProdutoById(Long id);
	List<PersonaProduto> getAllPersonaProduto();
	void deleteAllPersonaProduto();
	void deletePersonaProdutoById(Long id);
	void insertListaProdutoNoPersona(Persona persona, ArrayList<Produto> listaProduto);
	List<PersonaProduto> buscarTodosProdutosDoPersona(Persona persona);

}
