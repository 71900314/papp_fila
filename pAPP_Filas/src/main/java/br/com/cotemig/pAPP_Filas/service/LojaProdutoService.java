package br.com.cotemig.pAPP_Filas.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.cotemig.pAPP_Filas.model.Loja;
import br.com.cotemig.pAPP_Filas.model.LojaProduto;
import br.com.cotemig.pAPP_Filas.model.Produto;

public interface LojaProdutoService {

	Optional<LojaProduto> getLojaProdutoById(Integer id);
	List<LojaProduto> getAllLojaProduto();
	void deleteLojaProdutoById(Integer id);
	void deleteAllLojaProduto();
	void insertListProdutoNaLoja(Loja loja, ArrayList<Produto> listProduto);
	List<LojaProduto> buscarTodosProdutosDaLoja(Loja loja);
}
