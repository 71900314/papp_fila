package br.com.cotemig.pAPP_Filas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import br.com.cotemig.pAPP_Filas.model.Persona;

@Repository("personaRepository")
public interface PersonaRepository extends JpaRepository<Persona, Long> {
	 
}