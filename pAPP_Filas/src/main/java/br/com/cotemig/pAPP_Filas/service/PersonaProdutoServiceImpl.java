package br.com.cotemig.pAPP_Filas.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cotemig.pAPP_Filas.model.Persona;
import br.com.cotemig.pAPP_Filas.model.PersonaProduto;
import br.com.cotemig.pAPP_Filas.model.Produto;
import br.com.cotemig.pAPP_Filas.repository.PersonaProdutoRepository;

@Service("personaProdutoService")
public class PersonaProdutoServiceImpl implements PersonaProdutoService{

	@Autowired
	PersonaProdutoRepository personaProdutoRepository;

	public void insertListaProdutoNoPersona(Persona persona, ArrayList<Produto> listaProduto) {
		
		PersonaProduto personaProdutoEx;
		
		for(Produto prodEx : listaProduto) {
			
			personaProdutoEx = new PersonaProduto();
			
			personaProdutoEx.setPersona(persona);
			personaProdutoEx.setProduto(prodEx);
			
			personaProdutoRepository.save(personaProdutoEx);
			
		}
		
	}
	
	@Override
	public Optional<PersonaProduto> getPersonaProdutoById(Long id) {
		return personaProdutoRepository.findById(id);
	}

	@Override
	public List<PersonaProduto> getAllPersonaProduto() {
		return personaProdutoRepository.findAll();
	}

	@Override
	public void deleteAllPersonaProduto() {
		personaProdutoRepository.deleteAll();
	}

	@Override
	public void deletePersonaProdutoById(Long id) {
		personaProdutoRepository.deleteById(id);
	}

	@Override
	public List<PersonaProduto> buscarTodosProdutosDoPersona(Persona persona) {
		
		ArrayList<PersonaProduto> listaPersonaProduto = new ArrayList<>();
		
		listaPersonaProduto.addAll(personaProdutoRepository.buscarProdutosDoPersona(persona.getId()));
		
		return listaPersonaProduto;
	}


}
