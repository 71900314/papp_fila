package br.com.cotemig.pAPP_Filas.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cotemig.pAPP_Filas.model.Loja;
import br.com.cotemig.pAPP_Filas.repository.LojaRepository;

@Service("lojaService")
public class LojaServiceImpl implements LojaService {

	@Autowired
	LojaRepository lojaRepository;
	
	@Override
	public Optional<Loja> getLojaById(Integer id) {
		return lojaRepository.findById(id);
	}

	@Override
	public List<Loja> getAllLoja() {
		return lojaRepository.findAll();
	}

	@Override
	public void deleteAllLoja() {
		lojaRepository.deleteAll();
	}

	@Override
	public void deleteLojaById(Integer id) {
		lojaRepository.deleteById(id);
	}

	@Override
	public void updateLojaById(Integer id, Loja loja) {
		
		Optional<Loja> getLoja = getLojaById(id);
		
		getLoja.get().setNome(loja.getNome());
		getLoja.get().setLogradouro(loja.getLogradouro());
		getLoja.get().setTelefone(loja.getTelefone());
		
		lojaRepository.save(loja);
	}

	@Override
	public void updateLoja(Loja loja) {
		lojaRepository.save(loja);		
	}

	@Override
	public void insertLoja(Loja loja) {
		lojaRepository.save(loja);
	}

}
