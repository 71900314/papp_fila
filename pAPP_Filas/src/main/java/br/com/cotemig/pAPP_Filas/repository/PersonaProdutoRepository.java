package br.com.cotemig.pAPP_Filas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.cotemig.pAPP_Filas.model.PersonaProduto;

@Repository("personaProdutoRepository")
public interface PersonaProdutoRepository extends JpaRepository<PersonaProduto, Long> {

	@Query(value = "SELECT * FROM PERSONA_PRODUTO WHERE FK_PERSONA = ?1", nativeQuery = true)
	List<PersonaProduto> buscarProdutosDoPersona(Long id);

}
