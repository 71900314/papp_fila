package br.com.cotemig.pAPP_Filas.service;

import java.util.List;
import java.util.Optional;

import br.com.cotemig.pAPP_Filas.model.Loja;

public interface LojaService {

	Optional<Loja> getLojaById(Integer id);
	List<Loja> getAllLoja();
	void deleteAllLoja();
	void deleteLojaById(Integer id);
	void updateLojaById(Integer id, Loja loja);
	void updateLoja(Loja loja);
	void insertLoja(Loja loja);
}
