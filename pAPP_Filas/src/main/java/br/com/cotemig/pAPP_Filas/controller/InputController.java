package br.com.cotemig.pAPP_Filas.controller;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.cotemig.pAPP_Filas.model.Persona;
import br.com.cotemig.pAPP_Filas.model.Produto;
import br.com.cotemig.pAPP_Filas.restController.BaseRestController;
import br.com.cotemig.pAPP_Filas.service.PersonaService;

@Controller
public class InputController extends BaseRestController{

	
	@Autowired
	private PersonaService personaService;
	
	
	@GetMapping("/form")
	public String form() {
		return "form";
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index() {
	        
	    ModelAndView mav = new ModelAndView("index");
	    mav.addObject("personas", personaService.getAllPersona());
	    
	    setListaProdutoEx(new ArrayList<Produto>());
	    return mav;
	 }
	
	@RequestMapping(value = "/persona/index", method = RequestMethod.GET)
	public ModelAndView indexPersona() {
	        
	    ModelAndView mav = new ModelAndView("indexPersona");
	    mav.addObject("personas", personaService.getAllPersona());
	    
	    setListaProdutoEx(new ArrayList<Produto>());
	    return mav;
	 }
	
	@RequestMapping(value = "/persona/insert", method = RequestMethod.GET)
	public ModelAndView insert() {
		return new ModelAndView("insert", "persona", new Persona());
	}	 
	 
	@RequestMapping(value = "/persona/insert", method = RequestMethod.POST)
	public String submitInsert(@Valid @ModelAttribute("persona")Persona persona, 
								BindingResult result, ModelMap model) {
	        
		if (result.hasErrors()) {
			return "error";
		}
		        
		personaService.insertPersona(persona);        
		return "redirect:/persona/index";
	}
	 
	@RequestMapping(value = "/persona/delete", method = RequestMethod.GET)
	public ModelAndView delete(Long id) {
	 
		return new ModelAndView("delete", "persona", personaService.getPersonaById(id).get());
	}
	 
	@RequestMapping(value = "/persona/delete", method = RequestMethod.POST)
	public String submitDelete(@Valid @ModelAttribute("persona")Persona persona,
								BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			return "error";
		}
		 
		personaService.deletePersonaById(persona.getId());    
		return "redirect:/persona/index";
	}
	
	@RequestMapping(value = "/persona/update", method = RequestMethod.GET)
	public ModelAndView update(Long id) {
	 
		return new ModelAndView("update", "persona", personaService.getPersonaById(id).get());
	}
	
	@RequestMapping(value = "/persona/update", method = RequestMethod.POST)
	public String submitUpdate(@Valid @ModelAttribute("persona")Persona persona,
	      BindingResult result, ModelMap model) {
		        
		if (result.hasErrors()) {
			return "error";
		}
		 
		personaService.updatePersona(persona);
		        
		return "redirect:/persona/index";
	}

	@RequestMapping(value = "/persona/read", method = RequestMethod.GET)
	public ModelAndView read() {
	        
	    ModelAndView mav = new ModelAndView("read");
	    mav.addObject("listaPeronas", personaService.getAllPersona());
	    return mav;
	}

}
