package br.com.cotemig.pAPP_Filas.restController;

import java.util.ArrayList;
import java.util.List;

import br.com.cotemig.pAPP_Filas.model.Loja;
import br.com.cotemig.pAPP_Filas.model.Persona;
import br.com.cotemig.pAPP_Filas.model.PersonaProduto;
import br.com.cotemig.pAPP_Filas.model.Produto;

public class BaseRestController {

	private List<Persona> personagem;
	private List<Produto> listaProduto;
	private List<Produto> listaProdutoEx;
	private List<Loja> listLoja;
	private List<Loja> listLojaEx;
	private List<PersonaProduto> listaPersonaProduto;
	
	private Persona personaEx;
	private Loja lojaEx;

	public BaseRestController() {
		setPersonagem(new ArrayList<Persona>());
		setListaProduto(new ArrayList<Produto>());
		setListaProdutoEx(new ArrayList<Produto>());
		setListLoja(new ArrayList<Loja>());
		setListLojaEx(new ArrayList<Loja>());
		setPersonaEx(new Persona());
	}

	public List<Loja> getListLoja() {
		return listLoja;
	}

	public void setListLoja(List<Loja> listLoja) {
		this.listLoja = listLoja;
	}

	public List<Loja> getListLojaEx() {
		return listLojaEx;
	}

	public void setListLojaEx(List<Loja> listLojaEx) {
		this.listLojaEx = listLojaEx;
	}

	public List<Persona> getPersonagem() {
		return personagem;
	}

	public void setPersonagem(List<Persona> personagem) {
		this.personagem = personagem;
	}
	
	public List<Produto> getListaProduto() {
		return listaProduto;
	}

	public void setListaProduto(List<Produto> produto) {
		this.listaProduto = produto;
	}

	public List<PersonaProduto> getListaPersonaProduto() {
		return listaPersonaProduto;
	}

	public void setListaPersonaProduto(List<PersonaProduto> listaPersonaProduto) {
		this.listaPersonaProduto = listaPersonaProduto;
	}

	public List<Produto> getListaProdutoEx() {
		return listaProdutoEx;
	}

	public void setListaProdutoEx(List<Produto> listaProdutoEx) {
		this.listaProdutoEx = listaProdutoEx;
	}

	public Persona getPersonaEx() {
		return personaEx;
	}

	public void setPersonaEx(Persona personaEx) {
		this.personaEx = personaEx;
	}
	
	public Loja getLojaEx() {
		return lojaEx;
	}

	public void setLojaEx(Loja lojaEx) {
		this.lojaEx = lojaEx;
	}
}
