package br.com.cotemig.pAPP_Filas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.cotemig.pAPP_Filas.model.Loja;

@Repository("lojaRepository")
public interface LojaRepository extends JpaRepository<Loja, Integer> {
	
}
