package br.com.cotemig.pAPP_Filas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import br.com.cotemig.pAPP_Filas.model.Produto;

@Repository("produtoRepository")
public interface ProdutoRepository extends JpaRepository<Produto, Integer> {

}
